import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
// import Timer from './pages/Timer'
// import Login from "./pages/Login";
import Home from "./pages/Home";
import Profil from './pages/Profil'
import About from './pages/About'
import Login from './pages/Login'
import {Route, Switch } from "react-router-dom";
export default function App() {
  return (
    <>
    <Switch>
      <Route path="/home" component={Home} exact/>
      <Route path="/about" component={About} />
      <Route path="/profil" component={Profil} />
      <Route path="/" component={Login}  />
    </Switch>
        </>
  );
}
