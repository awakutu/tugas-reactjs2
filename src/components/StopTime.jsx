import React from "react";

export default function StopTime(count) {
  return clearInterval(count);
}
