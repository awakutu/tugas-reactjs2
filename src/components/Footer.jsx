import React from "react";

export default function Footer() {
  return (
    <footer class="page-footer font-small blue bg-dark">
      <div class="footer-copyright text-center  text-white py-3">
        © 2020 Copyright:
        <a href="https://gitlab.com/awakutu"> Rizky Tri Sulistyo</a>
      </div>
    </footer>
  );
}
