import React, { Component, Fragment } from "react";
import StartTime from "../components/StartTime";
import StopTime from "../components/StopTime";

export default class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      title: "",
    };
    this.onStart = this.onStart.bind(this);
    this.onStop = this.onStop.bind(this);
  }

  onStart() {
    this.timer = setInterval(() => {
      this.setState({
        count: StartTime(this.state.count),
      });
    }, 1000);
  }
  

  onStop() {
    StopTime(this.timer);
  }
  componentDidMount() {
    this.judul = this.setState({
      title: "Count Up Timer",
    });
  }

  componentWillUnmount() {
    console.log("Timer.componentWillUnmount");
  }

  render() {
    const { count, title } = this.state;
    return (
      <Fragment>
      <div className="container text-center">
        <h1>{title}</h1>
        <h1>{count}</h1>
        <button className="mx-2 bg-primary text-white" onClick={this.onStart}>
          Start
        </button>
        <button className="bg-danger text-white" onClick={this.onStop}>
          Stop
        </button>
      </div>
      </Fragment>
    );
  }
}
