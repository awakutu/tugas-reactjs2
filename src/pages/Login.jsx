import React, { useState } from "react";
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  function validateForm() {
    return username.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    this.props.history.push("/home");
  }

  return (
    <>
      <form onSubmit={handleSubmit} action="">
        <div class="sidenav">
          <div class="login-main-text">
            <h2>
              Application
              <br />
              Login Page
            </h2>
            <p>Login or register from here to access.</p>
          </div>
        </div>
        <div class="main">
          <div class="col-md-6 col-sm-12">
            <div class="login-form login">
              <form>
                <FormGroup controlId="username" bsSize="large">
                  <FormLabel>Email</FormLabel>
                  <FormControl
                    autoFocus
                    type="username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                  />
                </FormGroup>
                <FormGroup controlId="password" bsSize="large">
                  <FormLabel>Password</FormLabel>
                  <FormControl
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    type="password"
                  />
                </FormGroup>
                <Link to="/home">
                  <Button
                    block
                    bsSize="large"
                    disabled={!validateForm()}
                    type="submit"
                  >
                    Login
                  </Button>
                </Link>
              </form>
            </div>
          </div>
        </div>
      </form>
    </>
  );
}
