import React from 'react'
import NavBar from "../components/NavBar";
import Footer from "../components/Footer";

export default function About() {
  return (
    <>
    <NavBar/>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-8 bg-white">
          <article id="profil">
            <h2>About</h2>
            <hr />
            <p>
             Lorem Ipsum             
            </p>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ligula est, aliquam id semper ut, efficitur eget neque. Suspendisse et mauris rutrum ante auctor
            </p>
          </article>
        </div>
      </div>
      <hr />
      <div class="container-fluid form">
        <form action="/action_page.php">
          <h4 class="message">Give me some messages</h4>
          <div class="form-group">
            <label for="email">First Name : </label>
            <input
              type="email"
              class="form-control"
              id="email"
              placeholder="Enter your first name "
            />
          </div>
          <div class="form-group">
            <label for="email">Last Name : </label>
            <input
              type="email"
              class="form-control"
              id="email"
              placeholder="Enter your last name"
            />
          </div>
          <div class="form-group">
            <label for="email">Email : </label>
            <input
              type="email"
              class="form-control"
              id="email"
              placeholder="Enter your email"
            />
          </div>
          <div class="form-group">
            <label for="pwd">Messages : </label>
            <textarea
              type="text"
              class="form-control"
              id="pwd"
              placeholder="Enter your password"
            ></textarea>
          </div>
          <div class="row align-items-center">
            <button type="submit" class="btn btn-primary mb-2">Submit</button>
          </div>
        </form>
      </div>
    </div>
    <Footer/>
    </>
  )
}
